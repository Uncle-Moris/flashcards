import sqlite3
from flask import Flask, render_template, request, flash,redirect, url_for, session

app = Flask(__name__)

app.config['SECRET_KEY'] = 'your secret key'


def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

@app.route('/')
def index():
    return render_template('home.html')


@app.route('/categories')
def categories():
    conn = get_db_connection()
    categories = conn.execute('SELECT * FROM categories').fetchall()
    conn.close()
    return render_template('index.html', categories=categories)

@app.route('/categories/add/', methods=['POST', 'GET'])
def add_category():
    if request.method == 'POST':
        name = request.form.get('name')
        if not name:
            flash('Category name is required')
        else:
            conn = get_db_connection()
            try:
                conn.execute('INSERT INTO categories (name) VALUES (?)', (name,))
                conn.commit()
                flash('Category added successfully!')
            except sqlite3.Error as e:
                flash(f'An error occurred: {e}')
            finally:
                conn.close()
            return redirect(url_for('index'))
    return render_template('categories/add.html')

@app.route('/flashcards/add/', methods=['GET', 'POST'])
def add_flashcard():
    conn = get_db_connection()
    categories = conn.execute("SELECT * FROM categories").fetchall()
    if request.method == 'POST':
        question = request.form.get('question')        
        answer = request.form.get('answer')
        category_id = request.form.get('category_id')

        if not question or not answer or not category_id:
            flash("Some data is missing")
        else:
            try:
                conn.execute('INSERT INTO flashcards (question, answer, category_id) VALUES (?,?,?)', (question, answer, category_id))
                conn.commit()
                flash('Flashcard added successfully!')
            except sqlite3.Error as e:
                flash(f'An error occurred: {e}')
            finally:
                conn.close()
            return redirect(url_for('index'))
    else:
        conn.close()
    return render_template('flashcards/add.html', categories=categories)

@app.route('/flashcards/<int:category_id>')
def flashcards_list(category_id):
    conn = get_db_connection()
    flashcards = conn.execute('''
        SELECT flashcards.id, flashcards.question, flashcards.answer, categories.name AS category_name
        FROM flashcards
        JOIN categories ON flashcards.category_id = categories.id
        WHERE flashcards.category_id = ?
    ''', (category_id,)).fetchall()
    conn.close()
    return render_template('flashcards/list.html', flashcards=flashcards)


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000)